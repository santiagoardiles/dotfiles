--[[
-- ~/.config/nvim/init.lua — Santiago Andrés Ardiles Páez
-- ------------------------------------------------------------------------ --]]

--[[
-- Global variables
----------------------------------- --]]
local g = vim.g
local o = vim.o
local opt = vim.opt

--[[
-- Clipboard
----------------------------------- --]]
o.clipboard = "unnamedplus"

--[[
-- User-interface
----------------------------------- --]]
o.cursorline = true
o.number = true
o.numberwidth = 2
o.signcolumn = "yes"

--[[
-- Text-editing
----------------------------------- --]]
o.autoindent = true
o.cindent = true
o.expandtab = true
o.list = true
o.listchars = "trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂"
o.relativenumber = true
o.shiftwidth = 4
o.smarttab = true
o.softtabstop = -1
o.tabstop = 4
o.textwidth = 300
o.wrap = true

--[[
-- Searching
----------------------------------- --]]
o.ignorecase = true
o.smartcase = true
