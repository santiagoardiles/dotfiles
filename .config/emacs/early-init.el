;;; early-init.el --- Early initialisation file -*- lexical-binding: t -*-

;; Copyright (c) 2022—2024  Santiago Andrés Ardiles Páez

;; Author: Santiago Andrés Ardiles Páez <hiya@santiagoandr.es>
;; URL: git.santiagoandr.es/Stows
;; Version: 0.1.0
;; Package-Requires: ((emacs "30.5"))

;;; Commentary:

;; This file is expected to be located at `~/.config/emacs/early-init.el'.

;;; Code:

;; Messing with `alloc' in order to start faster.
(setq gc-cons-threshold most-positive-fixnum gc-cons-percentage 0.5)

(tool-bar-mode -1)

(scroll-bar-mode -1)

(setq-default visible-bell 1)

(dolist (var '(default-frame-alist initial-frame-alist))
  (add-to-list var '(width . (text-pixels . 1200)))
  (add-to-list var '(height . (text-pixels . 900))))

(setq-default inhibit-splash-screen t)

;; (setq-default package-quickstart t)

;; Returning garbage collection threshold back to normal after initialisation.
(add-hook 'emacs-startup-hook
          (lambda () (setq gc-cons-threshold (* 1000 1000 8) gc-cons-percentage 0.1)))

(provide 'early-init)
;;; early-init.el ends here
