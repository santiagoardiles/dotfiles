;;; init.el --- Initialisation file -*- lexical-binding: t -*-

;; Copyright (c) 2022—2024  Santiago Andrés Ardiles Páez

;; Author: Santiago Andrés Ardiles Páez <hiya@santiagoandr.es>
;; URL: git.santiagoandr.es/Stows
;; Version: 0.1.0
;; Package-Requires: ((emacs "30"))

;;; Commentary:

;; This file is expected to be located at `~/.config/emacs/init.el'.  Changes
;; to face attributes (calls to `set-face-attribute') must be after
;; `load-theme'.

;;; Code:

(funcall (lambda ()
           (require 'package)
           (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
           (package-initialize)))

(funcall (lambda () (require 'use-package) (setq-default use-package-always-ensure t)))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; `simple.el'
(add-hook 'overwrite-mode-hook (lambda () (setq cursor-type (if (not overwrite-mode) t 'hbar))))

;; `cus-edit.el'
(setq-default custom-file (locate-user-emacs-file "custom-variables.el"))

(use-package modus-themes
  :straight (:host github :repo "protesilaos/modus-themes")
  :custom (modus-themes-italic-constructs t)
  ;; :init (load-theme 'modus-vivendi t)
  )

(use-package ef-themes
  :straight (:host github :repo "protesilaos/ef-themes")
  :custom (ef-themes-italic-constructs t))

;; Load one theme at random
(defun santi-load-random-theme (themes)
  "Load a theme from a list of THEMES (names) randomly."
  (let ((theme (nth (random (length themes)) themes))) (load-theme theme t)))

(santi-load-random-theme (append modus-themes-collection
                                 ;; ef-themes-collection
                                 ))

;; `faces.el'
(mapc (lambda (face) (set-face-attribute face nil :height 120)) '(mode-line mode-line-inactive))

;; `novice.el'
(setq-default disabled-command-function nil)

(use-package simple
  :ensure nil
  :init
  (setq-default column-number-mode t
                size-indication-mode t
                indent-tabs-mode nil
                completion-auto-select nil)
  (global-visual-line-mode))

;; `paren.el'
(setq-default show-paren-context-when-offscreen 'overlay)

;; Configuration for the C library `xdisp'
(setq-default frame-title-format '(("%* %f — ") . 'mode-name))
;; (setq-default display-line-numbers 'relative)
(setq-default display-line-numbers-type 'relative)

;; Configuration for the C library `fns'
(setq-default use-short-answers t)

;; `files.el'
;; (auto-save-visited-mode)
;; (setq-default auto-save-visited-interval 128)
(setq-default confirm-kill-processes nil)
(setq-default make-backup-files nil)
(setq-default backup-inhibited nil)
(setq-default require-final-newline 'visit-save)

(use-package files
  :ensure nil
  :config (setq-default confirm-kill-processes nil
                        make-backup-files nil
                        backup-inhibited nil
                        require-final-newline 'visit-save))

;; (setq-default create-lockfiles nil)

;; (setq-default auto-save-timeout 8)

;; `savehist.el'
(savehist-mode)

;; `autorevert.el'
(global-auto-revert-mode)

;; `delsel.el.el'
(delete-selection-mode)

;; --------------------
;; Is fido really better? Yes. 🗿
;; --------------------
;; `icomplete.el'
(fido-vertical-mode)
;; --------------------

;; Configuration for the C library `minibuf'
(setq-default read-buffer-completion-ignore-case t)
(setq-default history-length 512)

;; `minibuffer.el'
(setq-default read-file-name-completion-ignore-case t)
(setq-default completion-styles '(basic partial-completion flex))

;; `indent.el'
(setq-default tab-always-indent 'complete)

;; `elec-pair.el'
(electric-pair-mode)
;; (setq-default electric-pair-preserve-balance nil)

;; `display-line-numbers.el'
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'org-mode-hook 'display-line-numbers-mode)

;; `hl-line.el'
(global-hl-line-mode)

;; `saveplace.el'
(save-place-mode)

;; ----------------------------------------
;; Key mappings; should be organised differently.
;; ----------------------------------------

;; --------------------
;; Shouldnt be global
;; --------------------
(global-set-key (kbd "M-n") 'forward-sexp)
(global-set-key (kbd "M-p") 'backward-sexp)
(global-set-key (kbd "<f5>") 'flymake-show-buffer-diagnostics)
(global-set-key (kbd "<f7>") 'project-find-regexp)
(global-set-key (kbd "<f8>") 'project-find-file)
(global-set-key (kbd "<f9>") 'project-find-dir)

;; --------------------
;; Really global mappings
;; --------------------
(global-set-key (kbd "C-<next>") 'next-window-any-frame)
(global-set-key (kbd "C-<prior>") 'previous-window-any-frame)
(global-set-key (kbd "C-S-<next>") 'next-buffer)
(global-set-key (kbd "C-S-<prior>") 'previous-buffer)
(global-set-key (kbd "<escape> <f7>") 'project-or-external-find-regexp)
(global-set-key (kbd "<escape> <f8>") 'project-or-external-find-file)
(global-set-key (kbd "<escape> <prior>") 'find-dired)

;; ----------------------------------------

(use-package display-fill-column-indicator :ensure nil :config (setq-default fill-column 80))

(use-package prog-mode
  :ensure nil
  :hook (prog-mode . whitespace-mode) (prog-mode . display-fill-column-indicator-mode))

(use-package recentf
  :ensure nil
  :init (recentf-mode)
  :config (setq-default recentf-max-saved-items 64) (global-set-key (kbd "C-x f") 'recentf-open))

(use-package whitespace
  :config (setq-default whitespace-action '(auto-cleanup)
                        whitespace-style (remove 'lines (default-value 'whitespace-style))))

(use-package org
  :config (setq org-support-shift-select t
                org-log-done 'note
                org-log-into-drawer 'LOGBOOK)
  (org-babel-do-load-languages 'org-babel-load-languages '((lisp . t)))
  :hook (org-mode . (lambda ()
                      (display-fill-column-indicator-mode -1)
                      (whitespace-mode)
                      (electric-quote-mode)
                      (abbrev-mode))))

(use-package eglot
  :hook (((c-mode-common
           clojure-mode
           clojurec-mode
           clojurescript-mode
           go-mode
           haskell-mode
           java-mode
           lua-mode
           php-mode
           prog-mode
           python-mode
           scala-mode
           sh-mode
           ;; rust-mode
           yaml-mode)
          . eglot-ensure))
  :custom
  (eglot-autoshutdown t)
  (eglot-events-buffer-size 0)
  (eglot-extend-to-xref nil)
  (eglot-ignored-server-capabilities
   '(:hoverProvider
     :documentHighlightProvider
     :documentFormattingProvider
     :documentRangeFormattingProvider
     :documentOnTypeFormattingProvider
     :colorProvider
     :foldingRangeProvider
     :inlayHintProvider)))

(use-package elisp-mode
  :ensure nil
  :hook (emacs-lisp-mode . (lambda () (setq-local fill-column 100
                                                  indent-tabs-mode nil))))

;; --------------------
;; What is Hyperbole? What could I use it for?
;; --------------------
;; (use-package hyperbole
;;   :straight (:host github :repo "rswgnu/hyperbole")
;;   :init (hyperbole-mode))
;; --------------------

(use-package minions
  :straight (:host github :repo "tarsius/minions")
  :config (minions-mode 1) (setq minions-prominent-modes '(flymake-mode glasses-mode)))

(use-package multiple-cursors
  :straight (:host github :repo "magnars/multiple-cursors.el")
  :config
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "<f11>") 'mc/mark-all-like-this)
  (global-set-key (kbd "<f12>") 'mc/edit-lines))

(use-package move-text
  :straight (:host github :repo "emacsfodder/move-text")
  :config (move-text-default-bindings))

(use-package copilot
  :straight (:host github :repo "copilot-emacs/copilot.el" :files ("dist" "*.el"))
  :config
  (define-key copilot-completion-map (kbd "<tab>") 'copilot-accept-completion)
  (define-key copilot-completion-map (kbd "TAB") 'copilot-accept-completion))

(use-package apheleia
  :straight (:host github :repo "radian-software/apheleia")
  :config
  (apheleia-global-mode)
  (setf (alist-get 'php-mode apheleia-mode-alist) '(pretty-php))
  (push '(pretty-php "pretty-php") apheleia-formatters))

(use-package corfu
  :functions (global-corfu-mode)
  :custom
  (corfu-auto t)
  (corfu-cycle t)
  (corfu-on-exact-match nil)
  (corfu-preselect 'prompt)
  (corfu-preview-current nil)
  (corfu-quit-at-boundary t)
  (corfu-quit-no-match t)
  (corfu-scroll-margin 4)
  :init (global-corfu-mode))

(use-package web-mode
  :config (setq-default web-mode-enable-auto-indentation nil))

(use-package slime
  :init (setq-default inferior-lisp-program "sbcl"))

(use-package markdown-mode)

(use-package haskell-mode)

(require 'cc-cmds)
(defun santi-c-mode-common-hook ()
  "Hook for configuring the major modes belonging to the CC Mode family."
  (c-set-style "linux")
  (setq-local tab-width 8
              fill-column 80
              indent-tabs-mode t)
  (c-toggle-auto-newline -1))
(add-hook 'c-mode-common-hook 'santi-c-mode-common-hook)


(use-package gnu-apl-mode
  :hook (gnu-apl-mode . (lambda () (setq-local tab-width 8
                                               fill-column 80
                                               indent-tabs-mode t))))

(use-package php-mode
  :functions php-set-style
  :config (setq-default php-mode-coding-style 'Default)
  :hook (php-mode . (lambda () (setq-local tab-width 4
                                           fill-column 100
                                           indent-tabs-mode nil
                                           c-basic-offset 4)
                      (c-toggle-auto-newline -1))))

(use-package yaml-mode
  :hook (yaml-mode . (lambda () (setq-local tab-width 4
                                            fill-column 100
                                            indent-tabs-mode nil
                                            yaml-indent-offset 4)
                       (display-line-numbers-mode)
                       (whitespace-mode))))

(use-package neon-mode
  :hook (neon-mode . (lambda () (setq-local tab-width 4
                                            fill-column 100))))

(use-package lua-mode
  :hook (lua-mode . (lambda () (setq-local tab-width 4
                                           fill-column 100
                                           indent-tabs-mode nil))))

(use-package sh-script
  :ensure nil
  :hook (sh-mode . (lambda () (setq-local tab-width 4
                                          fill-column 100
                                          indent-tabs-mode t))))

(use-package neon-mode
  :straight (:host github :repo "Fuco1/neon-mode")
  :hook (neon-mode . (lambda () (setq-local indent-tabs-mode t))))

(use-package python
  :ensure nil
  :hook (python-mode . (lambda () (setq-local tab-width 4
                                              fill-column 100
                                              indent-tabs-mode nil
                                              c-basic-offset 4)
                         (c-toggle-auto-newline -1))))

(use-package go-mode
  :hook (go-mode . (lambda () (setq-local tab-width 4
                                          fill-column 100
                                          indent-tabs-mode t))))

(use-package tex-mode :ensure nil
  :hook (latex-mode . (lambda () (setq-local tab-width 4
                                             fill-column 100
                                             indent-tabs-mode nil)
                        (display-line-numbers-mode)
                        (whitespace-mode))))

(use-package rust-mode
  :straight (:host github :repo "rust-lang/rust-mode")
  :hook (rust-mode . (lambda () (setq-local tab-width 4
                                            fill-column 100
                                            indent-tabs-mode t))))

(use-package typescript-mode
  :straight (:host github :repo "emacs-typescript/typescript.el")
  :hook (rust-mode . (lambda () (setq-local tab-width 4
                                            fill-column 100
                                            indent-tabs-mode nil))))

(defun santi-display-startup-time ()
  "Prints start-up time information in the echo area."
  (message "Emacs loaded in %s with %d garbage collections."
           (format "%.2f seconds" (float-time (time-subtract after-init-time before-init-time)))
           gcs-done))

(add-hook 'emacs-startup-hook 'santi-display-startup-time)

(provide 'init)
;;; init.el ends here
