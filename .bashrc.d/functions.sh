# ------------------------------------------------------------------------------
# ~/.bashrc.d/functions.sh — Santiago Andrés Ardiles Páez
# ------------------------------------------------------------------------------

function start_stowy_session() {
	readonly NEW_LINE=$(echo -ne '\015')

	if [[ $PWD != $HOME ]]; then
		echo "Please, run this function from your home directory."
		exit 1
	fi

	screen -d -m -S stowy-session
	screen -S stowy-session -p 0 -X stuff "cd $HOME/Desktop$NEW_LINE"
	screen -S stowy-session -X screen 1
	screen -S stowy-session -p 1 -X stuff "cd $HOME/Stows$NEW_LINE"
	screen -r stowy-session
}
